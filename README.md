# Data quality tools in R user group

Join our user group about R packages for data quality assessments! Through monthly meetings, we aim to start an exchange about available tools for data quality assessments, address open questions and provide support for users of our own package, dataquieR. 

## Dates and topics

We will have our first meeting at 15 April 2024, 4.30 pm.       

## Contact

You can use [issues](https://gitlab.com/libreumg/data-quality-tools-r-users/-/issues) to ask a question, or join an ongoing discussion. For further inquiries, please contact dataquier (at) uni-greifswald (dot) de.